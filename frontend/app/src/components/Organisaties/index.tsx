import React from "react";
import {Outlet} from "react-router-dom";

// Todo: remove, not used since react-router v6

const Organisaties = () => (
	<Outlet />
);

export default Organisaties;