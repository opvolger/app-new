import Afspraak from "./Afspraak";
import Banktransactie from "./Banktransactie";
import Betaalinstructie from "./Betaalinstructie";
import Burger from "./Burger";
import Journaalpost from "./Journaalpost";
import Organisatie from "./Organisatie";
import Rekening from "./Rekening";

export {
	Afspraak,
	Banktransactie,
	Betaalinstructie,
	Burger,
	Journaalpost,
	Organisatie,
	Rekening,
};
